package ru.pisarev.tm.model;

import ru.pisarev.tm.api.entity.IWBS;
import ru.pisarev.tm.enumerated.Status;

import java.util.Date;

public class Project extends AbstractEntity implements IWBS {

    private String name;

    private String description = "";

    private Status status = Status.NOT_STARTED;

    private String userId = "";

    private Date startDate;

    private Date finishDate;

    private Date created = new Date();

    public Project(String name) {
        this.name = name;
    }

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getCreated() {
        return created;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }

}
