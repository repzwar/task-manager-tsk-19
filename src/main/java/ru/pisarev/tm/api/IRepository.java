package ru.pisarev.tm.api;

import ru.pisarev.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    List<E> findAll();

    void addAll(final Collection<E> collection);

    void add(final E entity);

    E findById(final String id);

    void clear();

    void removeById(final String id);

    void remove(final E entity);

}
