package ru.pisarev.tm.api.repository;

import ru.pisarev.tm.api.IRepository;
import ru.pisarev.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    List<Task> findAll(final String userId);

    List<Task> findAll(final String userId, final Comparator<Task> comparator);

    List<Task> findAllTaskByProjectId(final String userId, final String projectId);

    void removeAllTaskByProjectId(final String userId, final String projectId);

    Task bindTaskToProjectById(final String userId, final String taskId, final String projectId);

    Task unbindTaskById(final String userId, final String id);

    Task findById(final String userId, final String id);

    Task findByName(final String userId, final String name);

    Task findByIndex(final String userId, final int index);

    void add(final String userId, final Task task);

    void remove(final String userId, final Task task);

    Task removeById(final String userId, final String id);

    Task removeByName(final String userId, final String name);

    Task removeByIndex(final String userId, final int index);

    void clear(final String userId);

    int getSize(final String userId);
}
