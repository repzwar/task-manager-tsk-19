package ru.pisarev.tm.api.service;

import ru.pisarev.tm.api.IService;
import ru.pisarev.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IService<Task> {

    List<Task> findAll(String userId);

    List<Task> findAll(String userId, Comparator<Task> comparator);

    Task findById(String userId, String id);

    Task findByName(String userId, String name);

    Task findByIndex(String userId, Integer index);

    Task add(String userId, Task task);

    void remove(String userId, Task task);

    Task removeById(String userId, String id);

    Task removeByName(String userId, String name);

    Task removeByIndex(String userId, Integer index);

    void clear(String userId);

    Task updateById(String userId, final String id, final String name, final String description);

    Task updateByIndex(String userId, final Integer index, final String name, final String description);

    Task startById(String userId, String id);

    Task startByIndex(String userId, Integer index);

    Task startByName(String userId, String name);

    Task finishById(String userId, String id);

    Task finishByIndex(String userId, Integer index);

    Task finishByName(String userId, String name);

}
