package ru.pisarev.tm.service;

import ru.pisarev.tm.api.repository.IProjectRepository;
import ru.pisarev.tm.api.service.IProjectService;
import ru.pisarev.tm.enumerated.Status;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.exception.empty.EmptyIndexException;
import ru.pisarev.tm.exception.empty.EmptyNameException;
import ru.pisarev.tm.exception.system.IndexIncorrectException;
import ru.pisarev.tm.model.Project;
import ru.pisarev.tm.model.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll(final String userId) {
        return projectRepository.findAll(userId);
    }

    @Override
    public List<Project> findAll(final String userId, final Comparator<Project> comparator) {
        if (comparator == null) return null;
        return projectRepository.findAll(userId, comparator);
    }

    @Override
    public Project findById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findById(userId, id);
    }

    @Override
    public Project findByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index > projectRepository.getSize(userId)) throw new IndexIncorrectException();
        return projectRepository.findByIndex(userId, index);
    }

    @Override
    public Project findByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findByName(userId, name);
    }

    @Override
    public Project add(final String userId, final Project project) {
        if (project == null) return null;
        projectRepository.add(userId, project);
        return project;
    }

    @Override
    public void remove(final String userId, final Project project) {
        if (project == null) return;
        projectRepository.remove(userId, project);
    }

    @Override
    public Project removeById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.removeById(userId, id);
    }

    @Override
    public Project removeByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return projectRepository.removeByIndex(userId, index);
    }

    @Override
    public Project removeByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeByName(userId, name);
    }

    @Override
    public void clear(final String userId) {
        projectRepository.clear(userId);
    }

    @Override
    public Project updateById(final String userId, final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findById(userId, id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findByIndex(userId, index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = projectRepository.findById(userId, id);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project startByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Project project = projectRepository.findByIndex(userId, index);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project startByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findByName(userId, name);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project finishById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = projectRepository.findById(userId, id);
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Override
    public Project finishByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Project project = projectRepository.findByIndex(userId, index);
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Override
    public Project finishByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findByName(userId, name);
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

}
