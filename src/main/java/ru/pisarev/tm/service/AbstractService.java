package ru.pisarev.tm.service;

import ru.pisarev.tm.api.IRepository;
import ru.pisarev.tm.api.IService;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    private IRepository<E> repository;

    public AbstractService(final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public void addAll(final Collection<E> collection) {
        if (collection == null || collection.isEmpty()) return;
        repository.addAll(collection);
    }

    @Override
    public void add(final E entity) {
        if (entity == null) return;
        repository.add(entity);
    }

    @Override
    public E findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public void removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        repository.removeById(id);
    }

    @Override
    public void remove(final E entity) {
        if (entity == null) return;
        repository.remove(entity);
    }
}
