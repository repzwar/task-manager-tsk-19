package ru.pisarev.tm.command.task;

import ru.pisarev.tm.command.TaskAbstractCommand;
import ru.pisarev.tm.exception.entity.TaskNotFoundException;
import ru.pisarev.tm.model.Task;
import ru.pisarev.tm.model.User;
import ru.pisarev.tm.util.TerminalUtil;

public class TaskStartByNameCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-start-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Start task by name.";
    }

    @Override
    public void execute() {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().startByName(user.getId(), name);
        if (task == null) throw new TaskNotFoundException();
    }
}
