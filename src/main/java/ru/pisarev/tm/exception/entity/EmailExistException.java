package ru.pisarev.tm.exception.entity;

import ru.pisarev.tm.exception.AbstractException;

public class EmailExistException extends AbstractException {

    public EmailExistException() {
        super("Error. Email already exist.");
    }

    public EmailExistException(String value) {
        super("Error. Email '" + value + "' already exist.");
    }

}
