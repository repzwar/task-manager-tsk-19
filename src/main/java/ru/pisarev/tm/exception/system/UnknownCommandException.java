package ru.pisarev.tm.exception.system;

import ru.pisarev.tm.constant.TerminalConst;
import ru.pisarev.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(String command) {
        super("Incorrect command '" + command + "'. Use " + TerminalConst.CMD_HELP + " for display list of terminal commands.");
    }

}
