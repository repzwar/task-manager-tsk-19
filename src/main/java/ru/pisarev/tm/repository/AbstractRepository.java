package ru.pisarev.tm.repository;

import ru.pisarev.tm.api.IRepository;
import ru.pisarev.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> entities = new ArrayList<>();

    @Override
    public List<E> findAll() {
        return entities;
    }

    @Override
    public void addAll(final Collection<E> collection) {
        if (collection == null) return;
        entities.addAll(collection);
    }

    @Override
    public void add(final E entity) {
        if (entity == null) return;
        entities.add(entity);
    }

    @Override
    public E findById(final String id) {
        for (E entity : entities) {
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @Override
    public void removeById(final String id) {
        final E entity = findById(id);
        if (entity == null) return;
        entities.remove(entity);
    }

    @Override
    public void remove(final E entity) {
        entities.remove(entity);
    }
}
